const { Router } = require('express');
const UserController = require('../controllers/UserController');
const RoleController = require('../controllers/RoleController');


const router = Router();

router.get('/users',UserController.index);
router.get('/users/:id',UserController.show);
router.post('/users',UserController.create);
router.get('/users/:id',UserController.show);
router.put('/users/:id', UserController.update);
router.put('/usersaddrole/:id', UserController.addRelationRole);
router.delete('/usersremoverole/:id', UserController.removeRelationRole);
router.delete('/users/:id', UserController.destroy);


router.get('/roles',RoleController.index);
router.post('/roles',RoleController.create);

module.exports = router;