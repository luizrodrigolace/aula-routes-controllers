const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
}, {
    timestamps: false
});

User.associate = function(models) {
    User.belongsTo(models.Role);
}

module.exports = User;